

# introduction

A simple app using Neo4j that can peron, location and the relations(like a is b's friend or a was born in b location) among them.

# files strucutre
```sh
app.js         # routes and restful api

/views
    index.ejs  # main page
    person.ejs # person page
    /partials
        footer.ejs  # common footer
        header.ejs  # common header
```


# how to run it   

**step 1 run neo4j through docker**  

```sh
docker run \
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    --env=NEO4J_ACCEPT_LICENSE_AGREEMENT=yes \
    --env=NEO4J_AUTH=none \
    neo4j:3.3.7
```


**step 2 install node dependencies**

```sh
npm install
```

**step 3 run the app**     

```
node app
```

navigate to the address http://localhost:3000/
(you can also navigate into neo4j browswer to see the graph nodes(localhost:7474))


# restful APIs

```sh
GET /                       # the main page

POST /person

POST /location

POST /friends/connect

POST /person/born

GET /person/:id
```

# demo pics  

graph-data1  
![graph-data1](./pics/graph-data1.jpg)

graph-data2(in table)  
![graph-data2](./pics/graph-data2.jpg)

simpleUI   
![simpleUI](./pics/simpleUI.jpg)

# Neo4j use cases    

```
Real Time Recommendations  
Master Data Management  
Fraud Detection  
Graph Based Search   
Network & IT-Operations   
Identity & Access Management

```  


